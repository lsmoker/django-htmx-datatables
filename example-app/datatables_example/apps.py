from django.apps import AppConfig


class DatatablesExampleConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "datatables_example"

    verbose_name = "htmx datatables example"
