from enum import Enum

from django.test import TestCase

from htmx_datatables.core import DataCell, DataRow
from htmx_datatables.helpers import (
    add_enum_to_context,
    camel_to_snake,
    extract_data_objs,
    extract_data_values,
)


class TestCameToSnake(TestCase):
    def test_single_word(self):
        self.assertEqual(camel_to_snake("Test"), "test")

    def test_multi_word(self):
        self.assertEqual(camel_to_snake("TestAlpha"), "test_alpha")

    def test_multi_word_and_delimiter(self):
        self.assertEqual(camel_to_snake("TestAlphaBravo", "-"), "test-alpha-bravo")


class TestAddEnumToContext(TestCase):
    def test_should_add_enum_to_context_with_string(self):
        # given
        class MyEnum(Enum):
            ALPHA = "alpha"
            BRAVO = "bravo"

        context = {"other": "stuff"}
        # when
        add_enum_to_context(MyEnum, context)
        # then
        expected = {"ALPHA": "alpha", "BRAVO": "bravo"}
        self.assertDictEqual(context["MyEnum"], expected)

    def test_should_add_enum_to_context_with_int(self):
        # given
        class MyEnum(Enum):
            ALPHA = 1
            BRAVO = 2

        context = {"other": "stuff"}
        # when
        add_enum_to_context(MyEnum, context)
        # then
        expected = {"ALPHA": 1, "BRAVO": 2}
        self.assertDictEqual(context["MyEnum"], expected)


class TestExtractData(TestCase):
    def test_should_extract_values(self):
        # given
        data = [DataRow(cells=(DataCell("alpha"), DataCell("bravo")))]
        # when
        result = extract_data_values(data)
        # then
        expected = [["alpha", "bravo"]]
        self.assertListEqual(result, expected)

    def test_should_extract_values_as_dict(self):
        # given
        data = [
            DataRow(
                cells=(
                    DataCell('<a href="#">alpha</a>', column_name="name"),
                    DataCell("blue", column_name="color"),
                )
            )
        ]
        # when
        result = extract_data_objs(data)
        # then
        expected = [{"name": '<a href="#">alpha</a>', "color": "blue"}]
        self.assertListEqual(result, expected)

    def test_should_extract_values_as_dict_and_strip_tags(self):
        # given
        data = [
            DataRow(
                cells=(
                    DataCell('<a href="#">alpha</a>', column_name="name"),
                    DataCell("blue", column_name="color"),
                )
            )
        ]
        # when
        result = extract_data_objs(data, strip_tags=True)
        # then
        expected = [{"name": "alpha", "color": "blue"}]
        self.assertListEqual(result, expected)

    def test_should_include_group_column(self):
        # given
        data = [
            DataRow(
                cells=(
                    DataCell("alpha", column_name="name"),
                    DataCell("blue", column_name="color"),
                ),
                group=DataCell("calm", column_name="mood"),
            )
        ]
        # when
        result = extract_data_objs(data)
        # then
        expected = [{"name": "alpha", "color": "blue", "mood": "calm"}]
        self.assertListEqual(result, expected)
