"""Helpers for htmx_datatables."""

import re
from enum import Enum
from typing import Any, Dict, List, Sequence

from django.utils import html

from .core import DataRow


def add_enum_to_context(enum_class: Enum, context: dict):
    """Add enum to a request context."""
    enum_dict = {o.name: o.value for o in enum_class}
    context[enum_class.__name__] = enum_dict


def camel_to_snake(name: str, delimiter: str = "_") -> str:
    """Transform string in camel case to snake case. Delimiter can be configured."""
    replace_regex = r"\1" + delimiter + r"\2"
    name = re.sub(r"(.)([A-Z][a-z]+)", replace_regex, name)
    return re.sub(r"([a-z0-9])([A-Z])", replace_regex, name).lower()


def extract_data_values(data: Sequence[DataRow]) -> List[List[Any]]:
    """Extract values from a data of a HtmxDataTableView.
    This can be helpful for testing.

    Args:
        - strip_tags: When enabled, will strip all HTML tags from values.
    """
    result = [[cell.value for cell in row.cells] for row in data]
    return result


def extract_data_objs(
    data: Sequence[DataRow], strip_tags: bool = False
) -> List[Dict[Any, Any]]:
    """Extract objects from data of a HtmxDataTableView.
    This can be helpful for testing.

    Args:
        - strip_tags: When enabled, will strip all HTML tags from values.
    """

    def _extract_value(obj) -> Any:
        if strip_tags:
            return html.strip_tags(obj.value)
        return obj.value

    rows = []
    for row in data:
        obj = {cell.column_name: _extract_value(cell) for cell in row.cells}
        if row.group:
            obj[row.group.column_name] = _extract_value(row.group)
        rows.append(obj)
    return rows
